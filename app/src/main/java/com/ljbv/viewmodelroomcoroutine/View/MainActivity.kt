package com.ljbv.viewmodelroomcoroutine.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.ljbv.viewmodelroomcoroutine.*
import com.ljbv.viewmodelroomcoroutine.Model.Producto
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val viewModel: ViewModelProducto by viewModels {
        ViewModelFactory((application as App).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listaProductos = emptyList<Producto>()

//        val database = Database.getDatabase(this)

        viewModel.productoAll.observe( this, Observer { producto ->
            listaProductos = producto

            val adapter = ProductoAdapter(this, listaProductos)

            lista.adapter = adapter

        })

//        database.productos().getAll().observe(this, Observer {
//            listaProductos = it
//
//            val adapter = ProductosAdapter(this, listaProductos)
//
//            lista.adapter = adapter
//        })

        lista.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, ProductoActivity::class.java)
            intent.putExtra("id", listaProductos[position].idProducto)
            startActivity(intent)
        }


        floatingActionButton.setOnClickListener {
            val intent = Intent(this, NewProductoActivity::class.java)
            startActivity(intent)
        }
    }
}