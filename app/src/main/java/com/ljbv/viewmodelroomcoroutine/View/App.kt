package com.ljbv.viewmodelroomcoroutine.View

import android.app.Application
import com.ljbv.viewmodelroomcoroutine.Model.DataBaseProducto
import com.ljbv.viewmodelroomcoroutine.ViewModel.Repository

class App: Application() {

    val database by lazy { DataBaseProducto.getDatabase(this) }

    val repository by lazy { Repository(database.productos()) }
}