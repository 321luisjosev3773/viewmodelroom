package com.ljbv.viewmodelroomcoroutine.View

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.ljbv.viewmodelroomcoroutine.*
import com.ljbv.viewmodelroomcoroutine.Model.Producto
import kotlinx.android.synthetic.main.activity_nuevo_producto.*

class NewProductoActivity : AppCompatActivity()  {

    private val viewModel: ViewModelProducto by viewModels {
        ViewModelFactory((application as App).repository)
    }
    val REQUEST_TAKE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nuevo_producto)

        var idProducto: Int? = null

        if (intent.hasExtra("producto")) {
            val producto = intent.extras?.getSerializable("producto") as Producto

            nombre_et.setText(producto.nombre)
            precio_et.setText(producto.precio.toString())
            descripcion_et.setText(producto.descripcion)
            idProducto = producto.idProducto
        }

//        val database = AppDatabase.getDatabase(this)

        save_btn.setOnClickListener {
            val nombre = nombre_et.text.toString()
            val precio = precio_et.text.toString().toDouble()
            val descripcion = descripcion_et.text.toString()

            val producto = Producto(nombre, precio, descripcion, R.drawable.ic_launcher_background)

            if (idProducto != null) {
//                CoroutineScope(Dispatchers.IO).launch {
                producto.idProducto = idProducto

                viewModel.Update(producto)
//                    database.productos().update(producto)
                this@NewProductoActivity.finish()
//                }

            } else {
//                CoroutineScope(Dispatchers.IO).launch {
//                    database.productos().insertAll(producto)

                viewModel.insert(producto)

                this@NewProductoActivity.finish()
//                }
            }
        }

        foto.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePicture->
                takePicture.resolveActivity(packageManager).also {
                    startActivityForResult(takePicture, REQUEST_TAKE)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE && resultCode == RESULT_OK){
            data?.extras?.let {
                val img = it.get("data") as Bitmap
                Imagen.setImageBitmap(img)
            }
        }
    }
}