package com.ljbv.viewmodelroomcoroutine.View

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.ljbv.viewmodelroomcoroutine.*
import com.ljbv.viewmodelroomcoroutine.Model.Producto
import kotlinx.android.synthetic.main.activity_producto.*

class ProductoActivity: AppCompatActivity()  {

    private lateinit var producto: Producto
    private lateinit var productoLiveData: LiveData<Producto>

    private val viewModel: ViewModelProducto by viewModels {
        ViewModelFactory((application as App).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_producto)

        //instacienado la base de datos
//        database = AppDatabase.getDatabase(this)

        //extrayendo el id del producto de la cache
        val idProducto:Int = intent.getIntExtra("id", 0)

        //extrayendo los productos de la base de datos
//        productoLiveData = database.productos().get(idProducto)
        productoLiveData = viewModel.geting(idProducto)



        //observar la lista y seteandolas a los text views
        productoLiveData.observe(this, Observer {
            producto = it

            nombre_producto.text = producto.nombre
            precio_producto.text = producto.precio.toString()
            detalles_producto.text = producto.descripcion
            imagen.setImageResource(producto.imagen)
        })
    }

    //menu principal
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.producto_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    //opciones del menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit_item -> {
                val intent = Intent(this, NewProductoActivity::class.java)
                intent.putExtra("producto", producto)
                startActivity(intent)
            }

            R.id.delete_item -> {
                productoLiveData.removeObservers(this)

//                CoroutineScope(Dispatchers.IO).launch {

                viewModel.dele(producto)
//                    database.productos().delete(producto)
                this@ProductoActivity.finish()
//                }
            }
        }

        return super.onOptionsItemSelected(item)
    }
}