package com.ljbv.viewmodelroomcoroutine.Model

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ljbv.viewmodelroomcoroutine.Model.Producto
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductoDao {
    @Query("SELECT * FROM productos ")
    fun getAll(): Flow<List<Producto>>

    @Query("SELECT * FROM productos WHERE idProducto = :id")
    fun get(id: Int): LiveData<Producto>

    @Insert
    suspend fun insertAll(vararg productos: Producto)

    @Update
    suspend fun update(producto: Producto)

    @Delete
    suspend fun delete(producto: Producto)
}