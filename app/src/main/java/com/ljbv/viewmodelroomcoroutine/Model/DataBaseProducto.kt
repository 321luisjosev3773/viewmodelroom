package com.ljbv.viewmodelroomcoroutine.Model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Producto::class], version = 1)
abstract class DataBaseProducto: RoomDatabase()  {

    abstract fun productos(): ProductoDao

    companion object {
        @Volatile
        private var INSTANCE: DataBaseProducto? = null

        fun getDatabase(context: Context): DataBaseProducto {
            val tempInstance = INSTANCE

            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DataBaseProducto::class.java,
                    "app_database"
                ).build()

                INSTANCE = instance

                return instance
            }
        }
    }
}