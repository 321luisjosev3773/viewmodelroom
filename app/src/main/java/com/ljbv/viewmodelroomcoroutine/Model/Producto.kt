package com.ljbv.viewmodelroomcoroutine.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "productos")
data class Producto(
    @ColumnInfo(name = "nombre")val nombre:String,
    @ColumnInfo(name = "precio")val precio: Double,
    @ColumnInfo(name = "descripcion")val descripcion: String,
    @ColumnInfo(name = "imagen")val imagen: Int,
    @PrimaryKey(autoGenerate = true)
    var idProducto: Int = 0
): Serializable
