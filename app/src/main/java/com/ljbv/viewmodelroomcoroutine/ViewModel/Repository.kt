package com.ljbv.viewmodelroomcoroutine.ViewModel

import androidx.lifecycle.LiveData
import com.ljbv.viewmodelroomcoroutine.Model.Producto
import com.ljbv.viewmodelroomcoroutine.Model.ProductoDao
import kotlinx.coroutines.flow.Flow

class Repository(private val dao: ProductoDao) {

    val allWords: Flow<List<Producto>> = dao.getAll()


    suspend fun insertar(producto: Producto) {
        dao.insertAll(producto)
    }

    suspend fun actualizar(producto: Producto){
        dao.update(producto)
    }

    fun obtener(id:Int): LiveData<Producto> {
        val respuesta: LiveData<Producto> = dao.get(id)
        return respuesta
    }

    suspend fun eliminar(producto: Producto){
        dao.delete(producto)
    }
}