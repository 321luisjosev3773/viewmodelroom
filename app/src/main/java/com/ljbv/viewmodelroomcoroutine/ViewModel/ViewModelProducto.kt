package com.ljbv.viewmodelroomcoroutine

import androidx.lifecycle.*
import com.ljbv.viewmodelroomcoroutine.Model.Producto
import com.ljbv.viewmodelroomcoroutine.ViewModel.Repository
import kotlinx.coroutines.launch

class ViewModelProducto(private val repository: Repository): ViewModel() {
    val productoAll: LiveData<List<Producto>> = repository.allWords.asLiveData()


    fun insert(producto: Producto) = viewModelScope.launch {
        repository.insertar(producto)
    }

    fun Update(producto: Producto) = viewModelScope.launch {
        repository.actualizar(producto)
    }


    fun geting(id:Int): LiveData<Producto> {
        val response: LiveData<Producto> = repository.obtener(id)
        return response
    }



    fun dele(producto: Producto) = viewModelScope.launch {
        repository.eliminar(producto)
    }
}

class ViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ViewModelProducto::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ViewModelProducto(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}